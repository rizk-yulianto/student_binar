package chalange;

import java.util.Scanner;

public class kalkulator {
	public static void main(String[] args) {
//		inisialisasi scanner untuk menu awal
		Scanner scanP = new Scanner(System.in);
//		inisiasi variabel untuk memilih menu
		int pilihM;
//		looping menu
		do {
			
			System.out.println("pilih menu kalkulator");
			System.out.println("1. menghitung luas bangun");
			System.out.println("2. menghitung luas volume");
			System.out.println("3. keluar");
//			mengambil input pengguna untuk dijadikan pilihan menu
			pilihM = scanP.nextInt();
//			case untuk memilih menu
		switch(pilihM) {
//		case 1 untuk menghitung luas bangun datar
		case 1:
//			memanggil method Luas
			Luas();
//			keluar case1
			break;
//		case 2 untuk menghitung volume bangun ruang
		case 2:
//		meemanggil method menghitung bangun ruang
			Volume();
//		keluar case2
			break;
//		case3 adalah menu exit
		case 3:
			 System.out.println("Keluar Program...");
			 System.out.println("Selamat Tinggal");
	         System.exit(0);
	         break;
//	    apabila user menginputkan selain dari yang sudah ditentukan
	    default:
	    	System.out.println("menu tidak valid silahkan pilih kembali");
	    	break;
			}	
//		cek apabila pengguna memilih keluar atau tidak
		}while(pilihM != 3);
	}
//	method penghitung Luas
	public static void Luas() {
//		inisiasi pemilihan bangun ruang yang akan dihitung
		Scanner scan = new Scanner(System.in);
		
		System.out.println("nomor");
//		mengambil masukan pengguna untuk dijadikan pilihan
		int pilihan = scan.nextInt();
//		scan.close();
//		switch untuk menghitung bangun ruang
		switch (pilihan) {
		case 1:
			System.out.println("menghitung Luas Persegi");
			System.out.println("masukkan panjang");
			int p = scan.nextInt();
			System.out.println("masukkan lebar");
			int l = scan.nextInt();
			int luas = p*l;
			System.out.println("luas persegi " + p + l + "adalah " +luas);
			break;
		case 2:
			System.out.println("menghitung Luas Lingkaran");
			System.out.println("masukkan Jari-Jari");
			double r = scan.nextInt();
			double luasL = 3.14 * r *r;
			System.out.println("luas Lingkaran " + r + "adalah " +luasL);
			break;
		case 3:
			System.out.println("menghitung Luas Segitiga");
			System.out.println("masukkan Alas");
			int alas = scan.nextInt();
			System.out.println("masukkan Tinggi");
			int tinggi = scan.nextInt();
			double luasS = 0.5*alas*tinggi;
			System.out.println("luas segitiga " + alas + tinggi + "adalah " +luasS);
		case 4:
			System.out.println("menghitung Luas Persegi Panjang");
			System.out.println("masukkan panjang");
			int pp = scan.nextInt();
			System.out.println("masukkan lebar");
			int lp = scan.nextInt();
			int luasP = pp*lp;
			System.out.println("luas persegi " + pp + " " + lp + "adalah " +luasP);
			break;
//		apabila user menginputkan menu yang bukan ditentukan
		default :
			System.out.println("menu invalid");
			break;
		}
	}
	public static void Volume() {
		Scanner scanv = new Scanner(System.in);
		System.out.println("masukkan pilihan menu");
		System.out.println("1 menghitung volume kubus");
		System.out.println("2 menghitung volume balok");
		System.out.println("3 menghitung volume tabung");
		System.out.println("nomor");
		int pilihanv = scanv.nextInt();
		
		switch (pilihanv) {
		case 1:
			System.out.println("menghitung volume kubus");
			System.out.println("masukkan panjang sisi");
			int s = scanv.nextInt();
			int volumeK = s^3;
			System.out.println("volume dari kubus dengan sisi " + s + " = "+ volumeK);
			break;
		case 2:
			System.out.println("menghitung volume balok");
			System.out.println("masukkan panjang");
			int pb = scanv.nextInt();
			System.out.println("masukkan tinggi");
			int tb = scanv.nextInt();
			System.out.println("masukkan lebar");
			int lb = scanv.nextInt();
			int vbalok = pb*tb*lb;
			System.out.println("volume balok adalah" + vbalok);
			break;
		case 3:
			System.out.println("menghitung volume tabung");
			System.out.println("masukkan tinggi");
			double tt = scanv.nextInt();
			System.out.println("masukkan jari jari alas");
			double ra = scanv.nextInt();
			double volT = (3.14 * tt * ra*ra);
			System.out.println("volume tabung adalah = " + volT);
			break;
		default:
			System.out.println("menu tidak valid");
			break;
		
		}
	}

}
