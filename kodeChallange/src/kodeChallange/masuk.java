package kodeChallange;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
//import java.util.List;
import java.util.Scanner;




public class masuk {
	public class read {
		 public static final String delimiter = ";";
		 
		   public static ArrayList<String> isi() {
			   String csvFile = "C:\\temp\\data_sekolah.csv";
			   ArrayList<String> content = new ArrayList<>();
			   String[] tempArr;
		      try {
		         File file = new File(csvFile);
		         FileReader fr = new FileReader(file);
		         BufferedReader br = new BufferedReader(fr);
		         String line = "";
		         
		         while((line = br.readLine()) != null) {
//		            tempArr = line.split(delimiter);
		        	 tempArr = line.split(";");
		            for(String tempStr : tempArr) {
						 content.add(tempStr.replaceAll("[^0-9]", ""));	 
					 }	 
		         }
		         br.close();
		         } catch(IOException ioe) {
//		            ioe.printStackTrace();
		        	 System.out.println("file tidak ada");
		        	 System.exit(0);
		        	 
		         }
		      return (content);
		   }
		   

		
	}
	


	   


	public static void main(String[] args) {

	      
		ArrayList<String> listInClassA = read.isi();
		
		

		Scanner scan = new Scanner(System.in);
		int pilih = 9;
		
		
		do {
			System.out.println("letakkan file di direktori C://temp/direktori");
			System.out.println("1. generate modus");
			System.out.println("2. generate median dan mean");
			System.out.println("3. generate 2-2 nya");
			System.out.println("0. keluar");
//			mengambil input pengguna untuk dijadikan pilihan menu
			pilih = scan.nextInt();
			
			switch (pilih) {
			case 1: {
				int modus = carimodus(listInClassA);
				System.out.println(modus);
				String modus1 = String.valueOf(modus);
				
				try {
				      FileWriter myWriter = new FileWriter("C:\\tmp\\modus.txt");
				      BufferedWriter bwr = new BufferedWriter(myWriter);
				      bwr.write("modus adalah");
				      bwr.newLine();
				      bwr.write(modus1);
				      bwr.flush();
				      bwr.close();
				      System.out.println("file dibuat dengan nama modus.txt di : C://temp/direktori");

				    } catch (IOException e) {
				      System.out.println("An error occurred.");
				      e.printStackTrace();
				    }
				break;
			}
			case 2: {
				float mean = carimean(listInClassA);
				float median = carimedian(listInClassA);
				System.out.println(median);
				System.out.println(mean);
				
				String mean1 = String.valueOf(mean);
				String median1 = String.valueOf(median);
				
				try {
				      FileWriter myWriter = new FileWriter("C:\\tmp\\mean-median.txt");
				      BufferedWriter bwr = new BufferedWriter(myWriter);
				      bwr.write("mean adalah");
				      bwr.newLine();
				      bwr.write(mean1);
				      bwr.newLine();
				      bwr.write("median adalah");
				      bwr.newLine();
				      bwr.write(median1);
				      bwr.flush();
				      bwr.close();
				      System.out.println("file dibuat dengan nama mean-median.txt di : C://temp/direktori");

				    } catch (IOException e) {
				      System.out.println("An error occurred.");
				      e.printStackTrace();
				    }
				
				break;
			}
			case 3:{
				int modus = carimodus(listInClassA);
				float mean = carimean(listInClassA);
				float median = carimedian(listInClassA);
				
				String mean1 = String.valueOf(mean);
				String median1 = String.valueOf(median);
				String modus1 = String.valueOf(modus);
				
				try {
				      FileWriter myWriter = new FileWriter("C:\\temp\\mean-median-modus.txt");
				      BufferedWriter bwr = new BufferedWriter(myWriter);
				      bwr.write("mean adalah");
				      bwr.newLine();
				      bwr.write(mean1);
				      bwr.newLine();
				      bwr.write("median adalah");
				      bwr.newLine();
				      bwr.write(median1);
				      bwr.newLine();
				      bwr.write("modus adalah");
				      bwr.newLine();
				      bwr.write(modus1);
				      bwr.flush();
				      bwr.close();
				      System.out.println("file dibuat dengan nama mean-median-modus.txt di : C://temp/direktori");

				    } catch (IOException e) {
				      System.out.println("An error occurred.");
				      e.printStackTrace();
				    }
				
				break;
			}
			case 0:{
				System.out.println("selamat tinggal");
				System.exit(0);
		        break;
			}
			default:
				System.out.println("pilihan tidak ada");
				break;
			}
			
		}while(pilih!=0);
	    
	       
	}
	
	public static float carimean(ArrayList<String> masuk) {
		Collections.sort(masuk);

		for(int i=0; i<8; i++) {
			masuk.remove(0);
		}
		
		int size = masuk.size();
		Integer[] intarray = null;
		intarray = new Integer[masuk.size()];
		
		try {
            for (int i = 0; i < masuk.size(); i++) {
                intarray[i] = (int) Float.parseFloat(masuk.get(i));
            }
        } catch (NumberFormatException e) {

        }

		 float sum = 0;
		    for (float x : intarray) {
		        sum += x;
		    }

		    float mean = sum / size;
		  
		return mean;
	}
	
	public static float carimedian(ArrayList<String> masuk) {
		Collections.sort(masuk);

		for(int i=0; i<8; i++) {
			masuk.remove(0);
		}
		
//		int size = masuk.size();
		Integer[] intarray = null;
		intarray = new Integer[masuk.size()];
		
		try {
            for (int i = 0; i < masuk.size(); i++) {
                intarray[i] = (int) Float.parseFloat(masuk.get(i));
            }
        } catch (NumberFormatException e) {

        }

		float median;
	    int totalElements = intarray.length;
	    if (totalElements % 2 == 0) {
	        float sumOfMiddleElements = intarray[totalElements / 2] +
	                                  intarray[totalElements / 2 - 1];
	        median = ((float) sumOfMiddleElements) / 2;
	    }else {
	    	median = (float) intarray[intarray.length / 2];
	    }
	    return median;
	}
	
	public static Integer carimodus(ArrayList<String> masuk) {
		Collections.sort(masuk);

		for(int i=0; i<8; i++) {
			masuk.remove(0);
		}
		
//		int size = masuk.size();
		Integer[] intarray = null;
		intarray = new Integer[masuk.size()];
		
		try {
            for (int i = 0; i < masuk.size(); i++) {
                intarray[i] = (int) Float.parseFloat(masuk.get(i));
            }
        } catch (NumberFormatException e) {

        }

		int maxKey = 0;
	    int maxCounts = 0;

	    int[] counts = new int[intarray.length];

	    for (int i=0; i < intarray.length; i++) {
	        counts[intarray[i]]++;
	        	if (maxCounts < counts[intarray[i]]) {
	            maxCounts = counts[intarray[i]];
	            maxKey = intarray[i];
	        	}
	        }
	    return maxKey;
	}

}
